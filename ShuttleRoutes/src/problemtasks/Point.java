package problemtasks;

/**
 * This class models a single point on the earth surface referenciable by its
 * gps coordinates. The class also has a property for calculating the distance
 * between it and any other point
 *
 * @author aladago
 */
public class Point {

    //vehicle's position
    private float latitude;
    private float longitude;

    /**
     *
     * @param latitude the latitude of this point
     * @param longitude the logitude of this point
     */
    public Point(float latitude, float longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     *
     * @return the latitude of this point
     */
    public float getLatitude() {
        return latitude;
    }

    /**
     *
     * @return the longitude of this point
     */
    public float getLongitude() {
        return longitude;
    }

    /**
     * computes the distance between this point and another point using Using
     * the Law of Cosines for Spherical Trigonometry The formular is retrieved
     * from
     * https://engineering.purdue.edu/ece477/Archive/2008/Spring/S08-Grp03/nb/datasheets/haversine.pdf
     *
     * @param latitude the latitude of the other point
     * @param longitude the longitude of the other point
     *
     * @return the distance between this point and the other point according to
     * cosines.
     */
    public double getDistanceFromOther(double latitude, double longitude) {
        //compute approximate radius of the earth at latitude 6[most of the points
        //of interest are are above 5 but below 6]. 
        //6378 is the Equatorial radius of the earth in km
        int EARTH_RADIUS = 6378 - (int) (21 * Math.sin(6));

        double longD = this.longitude - longitude;
        double a = Math.sin(degree2Rad(this.latitude)) * Math.sin(degree2Rad(latitude));
        double b = Math.cos(degree2Rad(this.latitude))
                * Math.cos(degree2Rad(latitude)) * Math.cos(degree2Rad(longD));
        double distance = Math.acos(a + b);

        return EARTH_RADIUS * distance;

    }

    /**
     * Convert gps points in degrees to radians for use Consine was computation
     *
     * @param coordinate
     * @return the radians equavalent of this coordiante
     */
    private double degree2Rad(double axisLength) {
        return axisLength * Math.PI / 180;
    }

}
