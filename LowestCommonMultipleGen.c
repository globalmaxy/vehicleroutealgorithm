#include <stdio.h>
#include<stdlib.h>

//return the lowest common multiple of the numbers from 1 to max
long lCMOfRange(int max){
	long product = 1;
        for (int i = 2; i <= max; i++) {
            long m = i * (max / i); //determine the highest multiple of i
            //if the multiple is not already a factor of the product thus far, 
            //update the product. 
            if (product % m != 0) {
                product *= m;
                //because some multiples can occur more than once, 
                //ensure to divide out all instances of duplicate multiplication.
                //eg. eg. the highest multiple of 2 within  10 is 10. But also, 
                //the highest multiple of 4 within 10 is 8 (2*4). the two must be 
                //divided out to ensure the lowest common multiple is obtained. 
                for (int x = i - 1; x > 1; x--) {
                    if (i % x == 0) {
                        product = product / x;
                    }
                }

            }
        }

        return product;
    }

//test of the algorithm
int main (){
int max = 20;
long lCMbelow20 = lCMOfRange(max);
printf("The lowest common multiple of all the numbers from 1 to %d is %lu ", max, lCMbelow20);
return 0;
}
